package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeControl extends CommandBase {
    private final IntakeSubsystem m_intakeSubsystem;

    public IntakeControl(IntakeSubsystem subsystem) {
        m_intakeSubsystem = subsystem;
        addRequirements(m_intakeSubsystem);

    }
    @Override
    public void execute() {
        m_intakeSubsystem.turnOnIntake(false);
    }

    @Override
    public void end(boolean interrupted) {
        m_intakeSubsystem.turnOffIntake();
    }
}