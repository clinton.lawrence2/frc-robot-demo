package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase {
    WPI_VictorSPX intakeVelco = new WPI_VictorSPX(4);
    WPI_VictorSPX intakeWheel = new WPI_VictorSPX(5);

    public void turnOnIntake(boolean reverse) {
        double motorSpeed = .5;

        if(reverse)
        {
            motorSpeed = -motorSpeed;
        }
        intakeVelco.set(-motorSpeed); // velco should spin opposite of the wheels
        intakeWheel.set(motorSpeed); 
    }

    public void turnOffIntake() {
        intakeVelco.set(0);
        intakeWheel.set(0);
    }

    public void reverseIntake() {
        turnOnIntake(true);
    }

}